
const FIRST_NAME = "Elisa";
const LAST_NAME = "Ceica";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
    /*if(value!==NaN) return parseInt(value);
    if(typeof(value)===String || typeof(value)===string) return parseInt(value);*/
    if(value==Infinity || value==(-Infinity)) return NaN;
    if(value>Number.MAX_SAFE_INTEGER || value<Number.MIN_SAFE_INTEGER) return NaN;
    if(isNaN(value)) return NaN;
    return parseInt(value);
    
}

module.exports = { 
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

